﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace tsgIniToXml
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog oDlg = new OpenFileDialog())
            {
                DialogResult iChoice = oDlg.ShowDialog(this);
                if (iChoice == DialogResult.OK)
                {
                    this.txtLocation.Text = oDlg.FileName;
                }
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            string xSourceFile = this.txtLocation.Text;

            if (!File.Exists(xSourceFile))
            {
                MessageBox.Show("The specified file does not exist", "Ini to Xml");
                return;
            }
            else if (!xSourceFile.ToUpper().EndsWith(".INI"))
            {
                MessageBox.Show("The specified file is not an ini file", "Ini to Xml");
                return;
            }

            string xNewFile = xSourceFile.Substring(0, xSourceFile.Length - 4) + "Config.xml";
            LMP.INI2XML.Convert(xSourceFile, xNewFile);
            MessageBox.Show("Successfully converted");
        }

    }
}
